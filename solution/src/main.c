#include "../include/mem_internals.h"
#include <stdio.h>
#include <assert.h>
#include <sys/mman.h>

#define MEM_PAGE_SIZE 4096
#define ARRAY_SIZE 10
#define SMALL_SIZE_ALLOCATION 10
#define LARGE_SIZE_ALLOCATION 3000

// Тест на успешное выделение памяти
static void malloc_success_test()
{
    int* arr1 = malloc(sizeof(int) * ARRAY_SIZE);
    short* arr2 = malloc(sizeof(short) * ARRAY_SIZE);
    char* arr3 = malloc(sizeof(char) * ARRAY_SIZE);

    assert(arr1 != NULL);
    assert(arr2 != NULL);
    assert(arr3 != NULL);
    printf("success_malloc_test passed\n");

    free(arr1);
    free(arr2);
    free(arr3);
}

// Тест на освобождение одного блока из нескольких выделенных
static void free_one_test()
{
    char* arr1 = malloc(SMALL_SIZE_ALLOCATION);
    char* arr2 = malloc(SMALL_SIZE_ALLOCATION);
    char* arr3 = malloc(SMALL_SIZE_ALLOCATION);

    free(arr2);
    assert(check_free(arr2));
    assert(!check_free(arr1));
    assert(!check_free(arr3));

    printf("free_one_test passed\n");

    free(arr1);
    free(arr3);
}

// Тест на освобождение двух блоков из нескольких выделенных
static void free_two_test()
{
    char* arr1 = malloc(SMALL_SIZE_ALLOCATION);
    char* arr2 = malloc(SMALL_SIZE_ALLOCATION);
    char* arr3 = malloc(SMALL_SIZE_ALLOCATION);

    free(arr1);
    free(arr3);
    assert(check_free(arr1));
    assert(check_free(arr3));
    assert(!check_free(arr2));

    printf("free_two_test passed\n");

    free(arr2);
}

// Тест на расширение памяти
static void memory_over_close_test()
{
    char* arr1 = malloc(LARGE_SIZE_ALLOCATION);
    char* arr2 = malloc(LARGE_SIZE_ALLOCATION);
    assert(arr2 - arr1 == LARGE_SIZE_ALLOCATION + 
        BLOCK_HEADER_SIZE);

    printf("memory_over_close_test passed\n");

    free(arr1);
    free(arr2);
}

// Тест на выделение нового участка памяти
static void memory_over_test()
{
    char* arr1 = malloc(LARGE_SIZE_ALLOCATION);
    void* between = mmap(arr1 + MEM_PAGE_SIZE - LARGE_SIZE_ALLOCATION, LARGE_SIZE_ALLOCATION, PROT_READ | PROT_WRITE, 
		MAP_ANONYMOUS | MAP_PRIVATE | MAP_FIXED, -1, 0);
    char* arr2 = malloc(LARGE_SIZE_ALLOCATION);
    
    assert(arr2 - arr1 != LARGE_SIZE_ALLOCATION + 
        BLOCK_HEADER_SIZE);
    printf("memory_over_test passed\n");

    free(arr1);
    free(arr2);
}

int main()
{
    malloc_success_test();
    free_one_test();
    free_two_test();
    memory_over_close_test();   
    memory_over_test();
    printf("All tests passed\n");
    
    return 0;
}

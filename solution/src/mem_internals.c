#include "../include/mem_internals.h"
#include <sys/mman.h>
#include <stdio.h>

#define MMAP_MIN_HEAP_SIZE 1024
#define PAGE_SIZE 4096

static void* head = NULL;  // Адрес первого блока (и начала кучи)
static void* next_heap_adr = NULL;  // Адрес следующего блока (вплотную)

// Получить размер из вместимости блока
block_size size_from_capacity (block_capacity cap) { 
	return (block_size) { cap.bytes + offsetof(struct block_header, contents) };
}

// Получить вместимость блока из размера 
block_capacity capacity_from_size (block_size sz) { 
	return (block_capacity) { sz.bytes - offsetof(struct block_header, contents) };
}

// Добавить блок
struct block_header* create_block(void* addr, block_capacity capacity) {
	struct block_header* block = addr;
	block->next = NULL;
	block->capacity.bytes = capacity.bytes;
	block->is_free = true;
	return block;
}

// Вставить блок с вместимостью capacity после блока prev_block
struct block_header* insert_block(struct block_header* prev_block, block_size size) {
	block_capacity capacity = capacity_from_size(size);
	prev_block->capacity.bytes -= size.bytes;
	struct block_header* new_block = create_block(prev_block->contents + 
		prev_block->capacity.bytes, capacity);
	new_block->next = prev_block->next;
	return new_block;
}

// Обеденить свободные блоки после блока block
void merge_blocks(struct block_header* block) {
	struct block_header* next = block->next;
	while (next && next->is_free && 
		(size_t)((void*)next - (void*)block->contents) == block->capacity.bytes) {
		block->capacity.bytes += next->capacity.bytes;
		next = next->next;
	}
	block->next = next;
}

size_t size_to_page(size_t size) {
	return (size / PAGE_SIZE + 1) * PAGE_SIZE;
}

// Выделить память для кучи (не вплотную)
void* create_heap(size_t size) {
	void* heap = mmap(NULL, size, PROT_READ | PROT_WRITE, 
		MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
	if (heap == MAP_FAILED) {
		return NULL;
	}
	next_heap_adr = heap + size;
	return heap;
}

// Расширить память кучи (вплотную)
void* expand_heap(size_t size) {
	void* heap = mmap(next_heap_adr, size, PROT_READ | PROT_WRITE, 
		MAP_ANONYMOUS | MAP_PRIVATE | MAP_FIXED, -1, 0);
	if (heap == MAP_FAILED) {
		return NULL;
	}
	next_heap_adr = heap + size;
	return heap;
}

// Найти свободный блок с размером больше required_block_size
struct block_header* find_free_block(block_size required_block_size) {	
	size_t heap_size = size_to_page(required_block_size.bytes);
	struct block_header* block = head;
	struct block_header* last = NULL;
	block_capacity required_block_capacity = capacity_from_size(required_block_size);
	while (block) {
		if (block->is_free) {
			merge_blocks(block);
			if (required_block_capacity.bytes <= block->capacity.bytes) {
				block_size remaining_size = { block->capacity.bytes - required_block_capacity.bytes };
				if (remaining_size.bytes >= BLOCK_MIN_CAPACITY) {
					block->next = insert_block(block, remaining_size);
				}
				break;
			}
		}
		last = block;
		block = block->next;
	}
	if (block)
		return block;
	void* heap = expand_heap(heap_size);
	if (!heap) {
		heap = create_heap(heap_size);
		if (!heap) {
			return NULL;
		}
	}
	block_size temp_size = { heap_size };
	block_capacity temp_capacity = capacity_from_size(temp_size);
	block = create_block(heap, temp_capacity);
	if(last) {
		last->next = block;
		if (last->is_free) {
			merge_blocks(last);
			block = last;
		}
	}
	if(!head)
		head = block;
	block = find_free_block(required_block_size);
	return block;
}

// Выделить size байт в куче
void* malloc(size_t size) {
	block_capacity required_block_capacity = { size };
	block_size required_block_size = size_from_capacity(required_block_capacity);
	struct block_header* block = find_free_block(required_block_size);
	if (block == NULL)
		return NULL;
	block->is_free = false;
	return (void*)block->contents;
}

// Освободить выделенную память
void free(void* addr) {
	if (addr) {
		size_t offset = offsetof(struct block_header, contents);
		struct block_header* block = (struct block_header*)((uint8_t*)addr - offset);
		if (block) {
			block->is_free = true;
			merge_blocks(block);
		}
	}
}

// Проверка удален ли блок памяти
int check_free(void* addr) {
	if (addr) {
		size_t offset = offsetof(struct block_header, contents);
		struct block_header* block = (struct block_header*)((uint8_t*)addr - offset);
		return block->is_free;
	}
	return false;
}

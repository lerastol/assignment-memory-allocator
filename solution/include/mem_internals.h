#ifndef MEM_INTERNALS

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#define BLOCK_MIN_CAPACITY 24
#define BLOCK_HEADER_SIZE 17

typedef struct { size_t bytes; } block_capacity;
typedef struct { size_t bytes; } block_size;

// Заголовок блока
struct __attribute__((__packed__)) block_header {
  struct block_header* next;
  block_capacity capacity;
  bool is_free;
  uint8_t contents[];
};



// Выделить size байт в куче
void* malloc(size_t size);

// Освободить выделенную память
void free(void* ptr);

// Проверка удален ли блок памяти
int check_free(void* ptr);

#endif
